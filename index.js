var express = require('express'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


/*
 * ���������� �������
 */

/*
 * 0 - ������� �������
 * 1 - ����
 * 2 - ������� �����������
 * 3 - ������ �����������
 * 4 - ���� ��������
 */
function Role() {
    this.conditionPlayer = 0; // 0 - �����, 1 - �� �����.
    this.role = 0; // 0 - ������, 1 - �����.
}
function Lobby(idUser, lobbyName, password, minPlayer, maxPlayer) {
    this.id = lobbies.length;
    this.idUser = idUser;
    this.lobbyName = lobbyName;
    this.password = password;
    this.minPlayer = minPlayer;
    this.maxPlayer = maxPlayer;
    this.users = [FindUserById(idUser)];
    this.usersGame = [];
    this.condition = 0;

    this.timerWaitPlayer = null;
    this.timerGame = null;
    this.dateTick = null;
    this.PlayerCountVote = [];

    this.CheckPlayer = function () {
        var index = [],
            flag = false;
        if (users.length === maxPlayer) {
            clearInterval(timerWaitPlayer);
            var maf = Math.round(users.length * 0.33);
            for (var i = 0; i < maf; ) {
                var rand = Math.floor(Math.random() * (users.length + 1));
                for (var j = 0; j < index.length; j++) {
                    if (rand === index[i]) {
                        flag = true;
                    }
                } if (!flag) {
                    index.push(rand);
                    i++;
                }
                flag = false;
            }
            for (var i = 0; i < index.length; i++) {
                this.usersGame[index[i]].role = 1;
            }
            this.GameLoop();
        }
    };
    this.Create = function () {
        timerWaitPlayer = setInterval(this.CheckPlayer, 1000);
    };
    this.GameLoop = function () {
        this.condition = 1;
        timerGame = setInterval(this.SwitchCondition, 60000);
    };
    this.SwitchCondition = function () {
        timerGame = new Date();
        switch (this.condition) {
            case 1:
                this.condition = 2;
                break;
            case 2:
                this.condition = 3;
                this.KillPlayer();
                this.ClearPlayerCountVote();
                break;
            case 3:
                this.condition = 1;
                this.KillPlayer();
                this.ClearPlayerCountVote();
                break;
            case 4:
                // �������� �����
                break;
        }
    };
    this.KillPlayer = function () {
        max = -1;
        index = -1;
        for (var i = 0; i < PlayerCountVote.length; i++) {
            if (max < PlayerCountVote[i]) {
                max = PlayerCountVote[i];
                index = i;
            }
        }
        if (index === -1) {
            index = Math.floor(Math.random() * (users.length + 1));
        }
        this.usersGame[index].conditionPlayer = 1;
    };
    this.ClearPlayerCountVote = function () {
        for (var i = 0; i < PlayerCountVote.length; i++) {
            if (this.usersGame[indexed].conditionPlayer === 0) {
                this.PlayerCountVote[i] = 0;
            } else {
                this.PlayerCountVote[i] = -2;
            }
        }
    };
}
var users = [],
    usersOnline = [],
    messages = [],
    lobbies = [];

/*
 * User
 */
app.post('/user/login', function (req, res) {
    console.log("user", req.body);
    var answer = Login(req.body.username, req.body.password);
    console.log("usersOnline", usersOnline);
    res.json(answer);
});
app.post('/user/logout', function (req, res) {
    LogOut(req.body.id, req.body.key);
    console.log(req.body);
    console.log(usersOnline);
    res.send('success');
});
app.get("/user/statistics", function (req, res) {
    var id = req.query.id;
    res.json(users[id]);
});



/*
 * Lobby
 */
app.get("/lobby/get", function (req, res) {
    res.json(lobbies);
});
app.post('/lobby/createlobby', function (req, res) {
    var lobby = CreateLobby(req.body.id_user, req.body.lobbyname, req.body.password, req.body.min_player, req.body.max_player);
    console.log(lobbies);
    res.json(lobby);
});
app.post('/lobby/editLobby', function (req, res) {
    var lobby = FindLobbyById(req.query.lobby_id, req.body.id_user, req.body.lobbyname, req.body.password, req.body.min_player, req.body.max_player);
    console.log(lobbies);
    res.json(lobby);
});
app.get('/lobby/inputlobby', function (req, res) {
    InputLobby(req.query.lobby_id, req.query.user_id, req.query.password_lobby);
    res.send("200");
});
app.get('/lobby/outlobby', function (req, res) {
    OutLobby(req.query.lobby_id, req.query.user_id);
    res.send("200");
});
app.get('/lobby/get_message', function (req, res) {
    GetMessage(req.query.lobby_id);
    res.send("200");
});
app.post('/lobby/send_mesage', function (req, res) {
    SendMessage(req.body.time, req.body.id_lobby, req.body.id_user, req.body.text);
    console.log(messages);
    res.send("200");
});
app.get('/lobby/vote', function (req, res) {
    VoteLobby(req.query.id_lobby, req.query.user_id, req.query.target_user_id);
});
app.get('/lobby/get_lobby_info', function (req, res) {
    res.json(FindLobbyById(req.query.id_lobby));
});



app.listen(8080, 'localhost', function () {
    console.log('start server');
    console.log('Init users from database');
    var text = fs.readFileSync('Users.txt', 'utf8');
    InitUser(text);
    console.log();
});















function FindLobbyById(idLobby) {
    for (var i = 0; i < lobbies.length; i++) {
        if (lobbies[i].id == idLobby) {
            return lobbies[i];
        }
    }
}
function FindUserById(id) {
    for (var i = 0; i < usersOnline.length; i++) {
        if (usersOnline[i].id == id) {
            return usersOnline[i];
        }
    }
}
function InitUser(data) {
    lines = data.split('\r\n');
    for (var i = 0; i < lines.length; i++) {
        users.push(
            {
                id: i,
                username: lines[i].split(':')[1].split(',')[0],
                password: lines[i].split(':')[2].split(',')[0],
                games_played: lines[i].split(':')[3].split(',')[0],
                games_win: lines[i].split(':')[4].split(',')[0],
                image: lines[i].split(':')[5].split(',')[0]
            });
    }
}
function Login(username, password) {
    for (var i = 0; i < users.length; i++) {
        if (users[i].username === username && users[i].password === password) {
            usersOnline.push(users[i]);
            return {
                id: i,
                key: i + username
            };
        }
    }
}
function LogOut(id, key) {
    for (var i = 0; i < usersOnline.length; i++) {
        if ((usersOnline[i].id == id) && (key == id + usersOnline[i].username)) {
            usersOnline.splice(i, 1);
            return;
        }
    }
}
function CreateLobby(idUser, lobbyName, password, minPlayer, maxPlayer) {
    var lobby = new Lobby(idUser, lobbyName, password, minPlayer, maxPlayer);
    lobbies.push(lobby);
    lobby.Create();
}
function EditLobby(idLobby, idUser, lobbyName, password, minPlayer, maxPlayer) {
    var lobby = FindLobbyById(idLobby);
    lobby.idUser = idUser;
    lobby.lobbyName = lobbyName;
    lobby.password = password;
    lobby.minPlayer = minPlayer;
    lobby.maxPlayer = maxPlayer;
    return lobby;
}
function VoteLobby(lobbyId, userId, targetUserIdInTheBestLobbyAndInTheBestGameAllTimesAndNations) {
    var lobby = FindLobbyById(lobbyId);
    for (var i = 0; i < lobby.users.length; i++) {
        if (lobby.users[i].id == userId) {
            for (var j = 0; j < lobby.users.length; j++) {
                if (lobby.users[j].id == targetUserIdInTheBestLobbyAndInTheBestGameAllTimesAndNations) {
                    lobby.PlayerCountVote[j] += 1;
                }
            }
        }
    }

}
/*
 * TODO: ������� ������ id �������������, � �� ����� �������������
 */
function InputLobby(lobbyId, userId, password) {
    var lobby = FindLobbyById(lobbyId);
    var user = FindUserById(userId);
    if (lobby.password == password) {
        if (lobby.users.length < lobby.maxPlayer) {
            lobby.users.push(user);
            lobby.usersGame.push(new Role());
            lobby.PlayerCountVote.push();
        }
    }
    console.log('Lobby', lobby);
}
function OutLobby(lobbyId, userId) {
    var lobby = FindLobbyById(lobbyId);
    console.log('Lobby', lobby);
    for (var i = 0; i < lobby.users.length; i++) {
        if (lobby.users[i].id == userId) {
            lobby.users.splice(i, 1);
            lobby.usersGame.splice(i, 1);
            lobby.PlayerCountVote.splice(i, 1);
            break;
        }
    }
    console.log(lobby);
}
function GetMessage(lobbyId) {
    var answer = [];
    for (var i = 0; i < messages.length; i++) {
        if (messages[i].lobbyId == lobbyId) {
            answer.push(messages[i]);
        }
    }
    console.log('answer = ', answer);
    return answer;
}
function SendMessage(date, lobbyId, userId, text) {
    var message = {
        time: date,
        lobbyId: lobbyId,
        userId: userId,
        text: text
    };
    messages.push(message);
}