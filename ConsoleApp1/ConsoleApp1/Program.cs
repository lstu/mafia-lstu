﻿using System;
using System.Collections.Specialized;
using System.Net;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                using (WebClient web = new WebClient())
                {
                    Console.WriteLine("1 - Логин\n" +
                        "2 - Логоут\n" +
                        "3 - Статистика\n" +
                        "4 - Получение списка лобби\n" +
                        "5 - Создание лобби\n" +
                        "6 - Войти в лобби\n" +
                        "7 - Выйти из лобби\n" +
                        "8 - Получить все сообщения\n" +
                        "9 - Отправить сообщение");
                    var pars = new NameValueCollection();
                    byte[] answer;
                    string s;
                    int key = Convert.ToInt32(Console.ReadLine());
                    switch (key)
                    {
                        case 1:
                            pars.Add("username", "doggy");
                            pars.Add("password", "doggie123");
                            answer = web.UploadValues("http://localhost:8080/user/login", pars);

                            pars.Clear();
                            pars.Add("username", "OtherDoggy");
                            pars.Add("password", "doggie");
                            answer = web.UploadValues("http://localhost:8080/user/login", pars);
                            break;
                        case 2:
                            pars.Add("id", "0");
                            pars.Add("key", "0doggy");
                            answer = web.UploadValues("http://localhost:8080/user/logout", pars);
                            break;
                        case 3:
                            web.QueryString.Clear();
                            web.QueryString.Add("id", "0");
                            s = web.DownloadString("http://localhost:8080/user/statistics");
                            break;
                        case 4:
                            web.QueryString.Clear();
                            s = web.DownloadString("http://localhost:8080/lobby/get");
                            break;
                        case 5:
                            pars.Clear();
                            pars.Add("id_user", "0");
                            pars.Add("lobbyname", "student");
                            pars.Add("password", "P@ssword");
                            pars.Add("min_player", "3");
                            pars.Add("max_player", "5");
                            answer = web.UploadValues("http://localhost:8080/lobby/createlobby", pars);
                            break;
                        case 6:
                            web.QueryString.Clear();
                            web.QueryString.Add("lobby_id", "0");
                            web.QueryString.Add("user_id", "1");
                            web.QueryString.Add("password_lobby", "P@ssword");
                            s = web.DownloadString("http://localhost:8080/lobby/inputlobby");
                            break;
                        case 7:
                            web.QueryString.Clear();
                            web.QueryString.Add("lobby_id", "0");
                            web.QueryString.Add("user_id", "1");
                            s = web.DownloadString("http://localhost:8080/lobby/outlobby");
                            break;
                        case 8:
                            web.QueryString.Clear();
                            web.QueryString.Add("lobby_id", "0");
                            s = web.DownloadString("http://localhost:8080/lobby/get_message");
                            break;
                        case 9:
                            pars.Clear();
                            pars.Add("time", DateTime.Now.ToString());
                            pars.Add("id_lobby", "0");
                            pars.Add("id_user", "1");
                            pars.Add("text", "Привет! Я мирный!");
                            answer = web.UploadValues("http://localhost:8080/lobby/send_mesage", pars);
                            break;
                    }
                }
                Console.WriteLine("Выполнил");
            }
        }
    }
}
